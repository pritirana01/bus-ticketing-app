const jwt =require('jsonwebtoken')
const userSecretKey="@123poiuq"
function userAuthenticationToken(req,res,next){
  const token=req.header('Authorization')
  if(!token){
    res.status(400).json("something went wrong")
  }
  else{
    jwt.verify(token,userSecretKey,(error,decoded)=>{ //passing two argument and one callback function in callback function it will check if there is any occur during verification otherwise decoded contain decoded information from token which mostly include payload data(jwt contain (header,payload,signature)) here payload is like email
        if(error){
            res.status(403).json({message:"token forbidden"})
        }
        else{
            req.user=decoded.email //extract value of email
            next()
        }
    })
  }
}
module.exports=userAuthenticationToken