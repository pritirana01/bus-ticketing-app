const jwt=require('jsonwebtoken')
const secretKey='@pri123poi'
function authenticationToken(req,res,next){
    const token=req.header('Authorization')
    if(!token){
        res.status(400).json({message:"unauthorized"})
    }
    else{
        jwt.verify(token,secretKey,(error,decoded)=>{
            if(error){
                return res.status(403).json({message:"Forbiden"})
            }
            else{
                req.user=decoded.email
                next()

            }
        })
    }
}
module.exports=authenticationToken