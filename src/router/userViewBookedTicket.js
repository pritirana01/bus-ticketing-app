const express=require("express")
const {Router}=require("express")
const router=Router()
const getBookTicketModel=require("../models/ticketDetailModel")
//const app=express()
router.use(express.json())
router.post("/viewBookedTicket",async(req,res)=>{
    const {Ticket_id}=req.body
    try{
           const fetchTicketdetails= await getBookTicketModel.findOne({Ticket_id})
           if(!fetchTicketdetails){
            res.status(400).json({message:"not found"})
           }
           return res.status(200).json(fetchTicketdetails)
    }
    catch{
          res.status(400).json({message:"internal server error"})
    }
 })
 module.exports=router
/*app.listen(4000,"localhost",()=>{
    console.log("here server is running for port 4000")
})*/