const express=require("express")
const {Router}=require("express")
const router=Router()
const getTicketDetailModel=require("../models/ticketDetailModel")
const getAuthenticationToken=require("../middleware/authorizationToken")
router.use(express.json()) //used to parse incoming request body with json payload
router.post("/viewTicketDetail",getAuthenticationToken,async(req,res)=>{
    const {Ticket_id}=req.body
    try{
         const fetchTicketDetails=await getTicketDetailModel.findOne({Ticket_id})
         if(!fetchTicketDetails){
            res.json(400).json({message:"ticket details not found"})
         }
         return res.status(200).json(fetchTicketDetails)

    }    
    catch(error){
                 res.status(400).json({message:"internal server error"})
    }
})
module.exports=router