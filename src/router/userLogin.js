const express=require("express")
const {Router}=require("express")
const router=Router()
const jwt=require('jsonwebtoken')
const userSecretKey="@123poiuq"
const getUserDetail=require("../models/userDetail")
//const router=express()
router.use(express.json())
router.post('/userLogin',async(req,res)=>{
const {Email_Address,Password}=req.body
try{
const userData=  await getUserDetail.findOne({Email_Address,Password})
if(!userData){
    res.status(400).json({message:"user data not found"})
}
else{
    const token=jwt.sign({email:getUserDetail.Email_Address},userSecretKey,{expiresIn:'5h'})
    res.status(200).json({message:"user login successful",token})
}
}
catch(error){
res.status(400).json({message:"error occured"})

}
})
module.exports=router
/*app.listen(4000,'localhost',()=>{
    console.log("server running for port 4000")
})*/