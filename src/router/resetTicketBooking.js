/*const express=require("express")
const app=express()
const getBusDetailModel=require("../models/busDetailModel")
const getTicketBookModel=require("../models/ticketDetailModel")
app.use(express.json())
app.post("/resetTickets",async(req,res)=>{
    const {Bus_id}=req.body
    try{
          const fetchTickets=await getTicketBookModel.deleteMany({Bus_id})
          
          for(const deleteAllTickets of fetchTickets){
            const {Bus_id,Seat_Number}=deleteAllTickets
            const updateSeats= await getBusDetailModel.updateOne({
               Bus_id,  
            },{
               $addToSet:{Available_Seats:Seat_Number}
            },{
               new:true
            })
          }

      
         /*const {Bus_id,Available_Seats,Seat_Number}=fetchTickets
             const updateSeats= await getBusDetailModel.updateOne({
                Bus_id,  
             },{
                $addToSet:{Available_Seats:Seat_Number}
             },{
                new:true
             })

          res.status(200).json({message:"reset details successfully"})
    }
    catch(error){
         res.status(400).json({message:"internal server error"})
    }
})
app.listen(4000,"localHost",()=>{
    console.log("here is server runnnig for port 4000")
})*/



const express = require("express");
const { Router } = require("express");
const router = Router();
const getBusDetailModel = require("../models/busDetailModel");
const getTicketBookModel = require("../models/ticketDetailModel");
const getAuthenticationToken = require("../middleware/authorizationToken");

router.use(express.json());

router.post("/resetTickets", getAuthenticationToken, async (req, res) => {
    const { Bus_id } = req.body;
    try {
        const fetchTickets = await getTicketBookModel.find({ Bus_id });

        const deletedResult = await getTicketBookModel.deleteMany({ Bus_id });

        for (const deletedTicket of fetchTickets) {
            const { Bus_id, Seat_Number } = deletedTicket;

        const tickets= await getBusDetailModel.updateOne(
                { Bus_id },
                { $addToSet: { Available_Seats: Seat_Number } },
                { new: true }
            );
            await tickets.save()
        }
         
        res.status(200).json({ message: "reset details and updated seats successfully" });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Internal server error" });
    }
});

module.exports = router;


/*app.listen(4000, "localhost", () => {
    console.log("Server is running on port 4000");
});*/
