/*const express = require("express");
const { Router } = require("express");
const router = Router();
const getBookTicketModel = require("../models/ticketDetailModel");
const getBusDetailModel = require("../models/busDetailModel");
const getAuthenticationToken = require("../middleware/userAuthenticationToken");

router.use(express.json());

router.post('/bookTicket', getAuthenticationToken, async (req, res) => {
    const { Email_Address, Bus_id, Seat_Number, Book_Status } = req.body;
    try {
        const alreadyTicketBooked = await getBookTicketModel.exists({
            Email_Address,
            Bus_id,
            Seat_Number,
            Book_Status: "Confirmed"
        });

        if (alreadyTicketBooked) {
            return res.status(400).json({ message: "Seat is already booked" });
        }

        const updatedBus = await getBusDetailModel.findOneAndUpdate(
            { Bus_id: Bus_id },
            { $pull: { Available_Seats: Seat_Number } },
            { new: true }
        );

        if (!updatedBus) {
            return res.status(400).json({ message: "Invalid Bus_id" });
        }

        if (Book_Status === "Confirmed") {
            const newBookTicket = new getBookTicketModel({
                Email_Address,
                Bus_id,
                Seat_Number,
                Book_Status
            });

            await newBookTicket.save();
        }

        res.status(200).json({ message: "Seat booked successfully" });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Internal server error" });
    }
});

module.exports = router;


app.listen(4000, "localhost", () => {
    console.log("Server is running on port 4000");
});*/



const express = require("express");
const { Router } = require("express");
const router = Router();
const getBookTicketModel = require("../models/ticketDetailModel");
const getBusDetailModel = require("../models/busDetailModel");
const getAuthenticationToken = require("../middleware/userAuthenticationToken");
const getdataValidate=require('../validator/ticketValidator')

router.use(express.json());

router.post('/bookTicket', getAuthenticationToken, async (req, res) => {
    const { Email_Address, Bus_id, Seat_Number} = req.body;
    try{
        await getdataValidate.validateAsync(req.body)
    }
    catch(error){
                        res.status(400).json({message:"data vaildation error",error:error.message})
    }

    try {
        const updatedBus = await getBusDetailModel.findOneAndUpdate(
            { Bus_id: Bus_id },
            { $pull: { Available_Seats: Seat_Number } },
            { new: true }
        );

        const newBookTicket = new getBookTicketModel({
            Email_Address,
            Bus_id,
            Seat_Number,
            Book_Status:"Confirmed"
        });

        await newBookTicket.save();

        res.status(200).json({ message: "Seat booked successfully" });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Internal server error" });
    }
});

module.exports = router;

