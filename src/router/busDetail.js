const express=require("express")
const {Router}=require("express")
const router=Router()
const getBusDetail=require("../models/busDetailModel")
const getAUthenticationToken=require("../middleware/userAuthenticationToken")
//const app=express()
router.use(express.json())
router.post('/busDetail',getAUthenticationToken,async(req,res)=>{
    const {Bus_id}=req.body
    try{
        const getAboutBus=await getBusDetail.findOne({Bus_id})
        res.status(200).json(getAboutBus)

    }
    catch{
        res.status(400).json({message:"some error occured not able to fetch details"})
    }
})
module.exports=router
/*app.listen(4000,"localhost",()=>{
console.log("server is running for port 4000")
})*/