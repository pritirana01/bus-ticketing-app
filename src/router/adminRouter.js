const express=require('express')
const {Router}=require('express')
const router=Router()
const adminModel=require('../models/adminDetailModel')
const { model } = require('mongoose')
router.use(express.json())
const bcrypt=require('bcrypt')

router.post('/adminSignup',async(req,res)=>{
const {Email_Address,Password}=req.body
try{
   // const saltgen=await bcrypt.genSalt(10)
   // const salt= await bcrypt.genSalt(saltgen)
    const hashPassword= await bcrypt.hash(Password,10)
const admin=new adminModel({
    Email_Address,
    Password:hashPassword
});
const checkAlreadyAdmin=await adminModel.findOne({Email_Address:admin.Email_Address
})
if(checkAlreadyAdmin){
    return res.status(200).json({message:"admin already exist"})

}
else{
    await admin.save()
    return res.status(200).json({message:"admin details inserted successfully"})
}

}
catch(error){
return res.status(400).json({message:"some error occured during admin detail insertion time"})
}
})
module.exports=router