const express=require("express")
const {Router}=require("express")
const router=Router()
const getTicketBookModel=require("../models/ticketDetailModel")
const getBusDetailModel=require("../models/busDetailModel")
//const app=express()
router.use(express.json())
router.post("/cancelTicket",async(req,res)=>{
   const {Ticket_id}=req.body
   try{
    const cancelTicket=  await getTicketBookModel.findOneAndDelete({Ticket_id})
      res.status(200).json({message:"Ticket cancelled successfully"})

const {Bus_id,Seat_Number,Book_Status}=cancelTicket  //using destructuring getting bus-d, seatnumber and status
if(Book_Status=="Confirmed"){
    const updatedBus=await getBusDetailModel.findOneAndUpdate({
        Bus_id                     //searching document having this bus id
    },
    {
        $addToSet:{Available_Seats:Seat_Number}    //used to put value in array if that is not already present
    },
    {
        new:true   //ensures that document will show updated data not previous one
    })
    if(!updatedBus){
        res.status(400).json({message:"wrong bus id"})
    }
}
res.status(200).json({message:"seat updated successfully"})

   }
   catch(error){
       res.status(400).json({message:"internal server error"})
   }
})
module.exports=router
/*app.listen(4000,"localhost",()=>{
    console.log("here server is running for port 4000")
})*/