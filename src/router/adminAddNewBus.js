/*const express = require("express");
const getbusDetail = require("../models/busDetailModel");
const getAuthenticationToken = require("../middleware/authorizationToken");
const router = express.Router();

router.use(express.json());

router.post('/newBus', getAuthenticationToken, async (req, res) => {
    const Email_Address = req.user;
    const { Bus_Name, Source_Location, Destination_Location, Total_Seats, Available_Seats } = req.body;

    try {
        const newBus = new getbusDetail({
            Email_Address,
            Bus_Name,
            Source_Location,
            Destination_Location,
            Total_Seats,
            Available_Seats
        });

        await newBus.save();
        return res.status(200).json({ message: "New Bus details inserted successfully" });
    } catch (error) {
        return res.status(400).json({ message: "Something went wrong", error: error.message });
    }
});

module.exports = router;*/

/*app.listen(4000,"localhost",()=>{
    console.log("server running for port number 4000")
})*/

const express = require('express');
const { Router } =require('express')
const busdetails = require('../models/busDetailModel');
const authenticationToken = require('../middleware/authorizationToken');
function insertBusFun(){
    const router = Router();

    router.post('/newBus', authenticationToken, async (req, res) => {
        const Email = req.user;
        console.log(Email)
        const { Bus_Name, Source_Location, Destination_Location, Total_Seats, Available_Seats } = req.body;

        try {
            const firstBus = new busdetails({
                Email,
                Bus_Name,
                Source_Location,
                Destination_Location,
                Total_Seats,
                Available_Seats

            });

            await firstBus.save();
            return res.status(200).json({ message:"new  bus inserted successfully" });
        } catch (error) {
            return res.status(400).json({ message: "some error occurred during post insertion time", error });
        }
    });
    return router;
}
    
module.exports = insertBusFun;
