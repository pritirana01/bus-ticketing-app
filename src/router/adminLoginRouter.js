const express=require('express')
const jwt=require('jsonwebtoken')
const secretKey='@pri123poi'
const {Router}=require('express')
const getadminModel=require('../models/adminDetailModel')
const router=Router()
router.use(express.json())
router.post('/adminLogin',async(req,res)=>{
const {Email_Address,Password}= req.body
try{

   const adminDetailMatch= await  getadminModel.findOne({Email_Address,Password})
   if(!adminDetailMatch)
     return  res.status(400).json({message:" login failed"})
    else{
        const token=jwt.sign({email:getadminModel.Email_Address },secretKey,{expiresIn:'5h'})
        return res.status(200).json({message:"login successfull",token})
    }
}
catch(error){
res.status(400).json({message:"some error occured",error})
}
})

module.exports=router