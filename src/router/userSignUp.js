const express=require('express')
//const app=express()
const {Router}=require('express')
const router=Router()
router.use(express.json())
const getUserDetailModel=require('../models/userDetail')
router.post('/userSignup',async(req,res)=>{
const {Name,Email_Address,Password,Contact_Number,Age,City}=req.body
try{
const newUser=new getUserDetailModel({
    Name,
    Email_Address,
    Password,
    Contact_Number,
    Age,
    City
})
await newUser.save()
res.status(200).json({message:"user detail inserted successfully",})
}
catch(error){
res.status(400).json({message:"internal server error",error:error.message})

}
})
module.exports=router
/*app.listen(4000,"localHost",()=>{
    console.log("here is server running for port 4000")
})*/
//module.exports=router