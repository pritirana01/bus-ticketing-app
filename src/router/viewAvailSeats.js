const express = require("express");
const {Router}=require("express")
const router=Router()
const getbusdetailsModel = require("../models/busDetailModel");
//const app = express();
router.use(express.json());

router.post('/viewAvailSeats', async (req, res) => {
    const { Bus_id } = req.body;
    try {
        console.log({ Bus_id });
        const getObjectBus = await getbusdetailsModel.aggregate([
            {
                $match: { Bus_id: Bus_id }
            }
        ]);
        if (getObjectBus.length > 0) {
        const availableSeats = getObjectBus[0].Available_Seats;
        const responseObj = { Available_Seats: availableSeats };// here em creating a new object having key Available_seats and put extract value from availableSeats
            res.json(responseObj);
            
            }    }
     catch (error) {
     //   console.error('Internal Server Error:', error);
        res.status(500).json({ message: 'Internal Server Error', error: error.message });
    }
});
module.exports=router

/*app.listen(4000, 'localhost', () => {
    console.log("Server is running on port 4000");
});*/
      /*  console.log(getObjectBus);

        if (getObjectBus.length > 0) {
            res.json(getObjectBus);
        } else {
            res.status(404).json({ message: 'Bus not found' });
        }*/
 


