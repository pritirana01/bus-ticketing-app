const mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/BusTicketing');

const useDetailSchema = new mongoose.Schema({
  Name: {
    type: String,
    required: true
  },
  Email_Address: {
    type: String,
    unique: true,
    required: true,
    match: /^[^\s@]+@gmail\.com$/
  },
  Password: {
    type: String,
    required: true
  },
  Contact_Number: {
    type: Number,
    required: true,
    validate:{
          validator:function(value){
            return /^\d{10}$/.test(value)
          },
    message:"contact number must be 10 digit"
        }
  },
  Age: {
    type: Date,
    required: true
  },
  Date: {
    type: Date,
    default: Date.now
  },
  City: {
    type: String,
    required: true
  }
});

const userDetailModel = mongoose.model("userDetailModel", useDetailSchema);

/*async function updateFieldNames() {
  try {
    await userDetailModel.updateMany({}, { $rename: { 'User_id': 'Password' } });
    console.log('Field names updated successfully.');
  } catch (error) {
    console.error('Error updating field names:', error);
  } finally {
    mongoose.connection.close();
  }
}

updateFieldNames();*/
module.exports = userDetailModel;
