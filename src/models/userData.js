const mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/BusTicketing');

const useDetailSchema = new mongoose.Schema({
  Name: {
    type: String,
    required: true
  },
  Email_Address: {
    type: String,
    unique: true,
    required: true
  },
  Password: {
    type: String,
    required: true
  },
  Contact_Number: {
    type: Number,
    required: true
  },
  Age: {
    type: Date,
    required: true
  },
  Date: {
    type: Date,
    default: Date.now
  },
  City: {
    type: String,
    required: true
  }
});

const userDataModel = mongoose.model("userDataModel", useDetailSchema);
module.exports = userDataModel;