const mongoose=require("mongoose")
const {v4:uuidv4}=require('uuid')
mongoose.connect("mongodb://127.0.0.1:27017/BusTicketing")
const ticketBookSchema=new mongoose.Schema({
    Ticket_id:{
        type:String,
        default:uuidv4,
        required:true
    },
    Email_Address: {
        type:String,
        required:true

    },
    Bus_id: {
        type:String,
        required:true
    },
    Seat_Number:{
          type:Number,
          required:true

    },
   Book_Status: {
               type:String,
               default:"unconfirmed",
               require:true
 
    }
})
const ticketData=mongoose.model("ticketData",ticketBookSchema)
module.exports=ticketData