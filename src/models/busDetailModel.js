const mongoose=require('mongoose')
 const {v4:uuidv4}=require('uuid')
mongoose.connect("mongodb://127.0.0.1:27017/BusTicketing")
const busDetailSchema=new mongoose.Schema({
    Email_Address:{
        type:String,
        required:false

    },
    Bus_id: {
           type:String,
           default:uuidv4,
           required:true
    },
    
        Bus_Name:{
            type:String,
            required:true
        },
    Source_Location:{
                type:String,
                required:true
    },
    Destination_Location:{
        type:String,
        required:true
    },
  Total_Seats:  {
       type:Number,
       required:true
    },
    Available_Seats:{
        type:Array,
        required:true
    }
})
const busDetails=mongoose.model('busDetails',busDetailSchema)
module.exports=busDetails