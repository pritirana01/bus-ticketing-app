const express = require('express');
const app = express();
const getAdminRouter = require('./router/adminRouter');
const getadminLoginRouter = require('./router/adminLoginRouter');
const getAddBusEndPoint = require('./router/adminAddNewBus'); 
const getAddBusEndPointfun=getAddBusEndPoint();
const getUserSignUp=require("./router/userSignUp")
const getUserLogin=require("./router/userLogin")
const getAllBusRouter=require("./router/userAllBus")
const getBusDetailRouter=require("./router/busDetail")
const getAvailSeatDetails=require("./router/viewAvailSeats")
const getBookSeat=require("./router/bookTicket")
const getcancelBookTicket=require("./router/cancelTicket")
const getuserViewBookedTicket=require("./router/userViewBookedTicket")
const getAdminViewTicketDetail=require("./router/adminViewTicketDetail")
const getResetTicketDetailsRouter=require("./router/resetTicketBooking")

//import both  middleware  for admin & user 
const getadminToken = require('./middleware/authorizationToken');
const getUsertoken=require('./middleware/userAuthenticationToken')


app.use(express.json())

app.use(getAdminRouter);
app.use(getadminLoginRouter);

app.use('/admin',getadminToken); //this middleware apply only where route is /admin


app.use('/admin',getAddBusEndPointfun); 
app.use('/admin',getAdminViewTicketDetail)
app.use('/admin',getResetTicketDetailsRouter)

app.use(getUserSignUp)
app.use(getUserLogin)
app.use('/user',getUsertoken)
app.use('/user',getAllBusRouter)
app.use('/user',getBusDetailRouter)
app.use('/user',getAvailSeatDetails)
app.use('/user',getBookSeat)
app.use('/user',getcancelBookTicket)
app.use('/user',getuserViewBookedTicket)


app.listen(3000, 'localhost', () => {
    console.log("Server is running on port 3000");
});