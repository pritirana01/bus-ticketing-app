const joi=require('joi')
const ticketSchemaValidator=joi.object({
    Email_Address:joi.string().email().required(),
    Bus_id:joi.string().required(),
    Seat_Number:number().required()

})
module.exports=ticketSchemaValidator